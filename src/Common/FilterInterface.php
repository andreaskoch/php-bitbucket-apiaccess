<?php

namespace BitbucketApiAccess\Common;

interface FilterInterface
{
    /**
     * Check whether the $repository matches the supplied $filter.
     *
     * @param mixed $targetObject The target object
     * @param array $condition An array of filter conditions
     *
     * @returns bool true if the $repository matches the supplied $filter; otherwise false
     */
    public function isMatch($targetObject, $condition);
}