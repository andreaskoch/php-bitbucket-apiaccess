<?php
use BitbucketApiAccess\Common\Group;

class GroupPrivilegesTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getPrivilegesForGroup_GroupSupplied_ResultIsNotEmpty()
    {
        // arrange
        $bitbucketApi = $this->getMockBuilder('BitbucketApiAccess\Common\BitbucketApi')
            ->disableOriginalConstructor()
            ->getMock();

        $bitbucketApi->expects($this->any())
            ->method('get')
            ->will(
                $this->returnValue(
                    array(
                        array(
                            "repo" => "2team/public2teamrepo",
                            "privilege" => "write",
                            "group" => array(
                                "owner" => array(
                                    "username" => "2team",
                                    "first_name" => "2 Team",
                                    "last_name" => "",
                                    "avatar" => "https://secure.gravatar.com/avatar.png",
                                    "is_team" => true
                                ),
                                "name" => "Developers",
                                "members" => [
                                    array(
                                        "username" => "buserbb",
                                        "first_name" => "B",
                                        "last_name" => "userbb",
                                        "avatar" => "https://secure.gravatar.com/avatar.png",
                                        "is_team" => false
                                    )
                                ],
                                "slug" => "developers"
                            ),
                            "repository" => array(
                                "owner" => array(
                                    "username" => "2team",
                                    "first_name" => "2 Team",
                                    "last_name" => "",
                                    "avatar" => "https://secure.gravatar.com/avatar/15827007bdb707832ded90a612750cfb?d=https%3A%2F%2Fdwz7u9t8u8usb.cloudfront.net%2Fm%2F6bc30a724121%2Fimg%2Fteam_no_avatar_32.png&s=32",
                                    "is_team" => true
                                ),
                                "name" => "public2teamrepo",
                                "slug" => "public2teamrepo"
                            )
                        )
                    )
            ));

        $privileges = new \BitbucketApiAccess\Common\GroupPrivileges($bitbucketApi);
        $group = new Group("sample-org", "sample-group");

        // act
        $result = $privileges->getPrivilegesForGroup($group);

        // assert
        $this->assertNotEmpty($result);
    }
}
