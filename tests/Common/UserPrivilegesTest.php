<?php
use BitbucketApiAccess\Common\User;
use BitbucketApiAccess\Common\Repository;

class UserPrivilegesTest extends PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function getPrivileges_UsersSupplied_ResultIsNotEmpty()
    {
        // arrange
        $bitbucketApi = $this->getMockBuilder('BitbucketApiAccess\Common\BitbucketApi')
            ->disableOriginalConstructor()
            ->getMock();

        $privileges = new \BitbucketApiAccess\Common\UserPrivileges($bitbucketApi);
        $users = array(
            new User("johndoe")
        );
        $repositories = array(
            new Repository("acme", "sample-repo")
        );

        // act
        $result = $privileges->getPrivileges($users, $repositories);

        // assert
        $this->assertNotEmpty($result);
    }
}
