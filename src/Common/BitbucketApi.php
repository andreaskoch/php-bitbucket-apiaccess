<?php

namespace BitbucketApiAccess\Common;

use Requests;
use SebastianBergmann\GlobalState\Exception;

class BitbucketApi
{
    /** @var string self::CREDENTIAL_CACHE_PATH The path to the credential cache */
    const CREDENTIAL_CACHE_PATH = ".credentials";

    /** @var string self::CREDENTIAL_CACHE_FIELD_USERNAME */
    const CREDENTIAL_CACHE_FIELD_USERNAME = "username";

    /** @var string self::CREDENTIAL_CACHE_FIELD_PASSWORD */
    const CREDENTIAL_CACHE_FIELD_PASSWORD = "password";

    /** @var string $username The current username */
    private $username;

    /** @var string $password The current password */
    private $password;

    /**
     * Creates a new instance of the BitbucketApi class
     *
     * @param string $username The username
     * @param string $password The password
     *
     * @throws \InvalidArgumentException If the supplied $username is null or empty
     * @throws \InvalidArgumentException If the supplied $password is null or empty
     */
    public function __construct($username, $password)
    {
        if (empty($username)) {
            throw new \InvalidArgumentException("The supplied username cannot be null or empty");
        }

        if (empty($password)) {
            throw new \InvalidArgumentException("The supplied password cannot be null or empty");
        }

        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Instantiates a new BitbucketApi instance with from the credential cache
     *
     * @throws \Exception If the credential cache could not be read
     * @returns BitbucketApi An instance of the BitbucketApi class with the credentials loaded from the credential cache
     */
    public static function fromConfig()
    {
        $credentialCacheContent = file_get_contents(self::CREDENTIAL_CACHE_PATH);
        $credentials = json_decode($credentialCacheContent, true);
        if (is_null($credentials)) {
            throw new \Exception("Could not de-serialize credential cache");
        }

        $username = $credentials[self::CREDENTIAL_CACHE_FIELD_USERNAME];
        $password = $credentials[self::CREDENTIAL_CACHE_FIELD_PASSWORD];

        return new BitbucketApi($username, $password);
    }

    /**
     * Gets a flag indicating whether the current user can login to the bitbucket api
     */
    public function canLogin()
    {
        $response = $this->get("user/");
        return !is_null($response); // if the response is not empty the user can login
    }

    /**
     * Save the current credentials to a configuration file.
     */
    public function saveCredentials()
    {
        $config = array(
            self::CREDENTIAL_CACHE_FIELD_USERNAME => $this->getUsername(),
            self::CREDENTIAL_CACHE_FIELD_PASSWORD => $this->getPassword(),
        );

        file_put_contents(self::CREDENTIAL_CACHE_PATH, json_encode($config, JSON_PRETTY_PRINT));
    }

    /**
     * Clear the current credential configuration file.
     */
    public function clearCredentials()
    {
        unlink(self::CREDENTIAL_CACHE_PATH);
    }

    /**
     * Issue a GET request to the specified api method
     *
     * @param string $apiMethodRoute The route for the api method (e.g.
     *                               "repositories/$accountName/$repositoryName/deploy-keys")
     *
     * @return array The de-serialized response
     * @throws Exception If the request was not successfull
     */
    public function get($apiMethodRoute)
    {
        $headers = array('Accept' => 'application/json');
        $options = array('auth' => array($this->getUsername(), $this->getPassword()));
        $response = Requests::get("https://bitbucket.org/api/1.0/$apiMethodRoute", $headers, $options);
        if (empty($response) || $response->success == false) {
            throw new \Exception($response->body);
        }

        $deSerializedResponse = json_decode($response->body, true);
        if (is_null($deSerializedResponse)) {
            // response could not be de-serialized
            return array();
        }

        return $deSerializedResponse;
    }

    /**
     * Issue a PUT request to the specified api method
     *
     * @param string $apiMethodRoute The route for the api method (e.g.
     *                               "repositories/$accountName/$repositoryName/deploy-keys")
     * @param mixed  $data           The data to put
     *
     * @return array The de-serialized response
     */
    public function put($apiMethodRoute, $data)
    {
        $headers = array('Accept' => 'application/json');
        $options = array('auth' => array($this->getUsername(), $this->getPassword()));
        $response = Requests::put("https://bitbucket.org/api/1.0/$apiMethodRoute", $headers, $data, $options);
        if (empty($response) || $response->success == false) {
            // the request was not successful
            return null;
        }

        $deSerializedResponse = json_decode($response->body, true);
        if (is_null($deSerializedResponse)) {
            // response could not be de-serialized
            return array();
        }

        return $deSerializedResponse;
    }

    /**
     * Issue a POST request to the specified api method
     *
     * @param string $apiMethodRoute The route for the api method (e.g.
     *                               "repositories/$accountName/$repositoryName/deploy-keys")
     * @param mixed  $data           The data to post
     *
     * @return array The de-serialized response
     */
    public function post($apiMethodRoute, $data)
    {
        $headers = array('Accept' => 'application/json');
        $options = array('auth' => array($this->getUsername(), $this->getPassword()));
        $response = Requests::post("https://bitbucket.org/api/1.0/$apiMethodRoute", $headers, $data, $options);
        if (empty($response) || $response->success == false) {
            // the request was not successful
            return null;
        }

        $deSerializedResponse = json_decode($response->body, true);
        if (is_null($deSerializedResponse)) {
            // response could not be de-serialized
            return array();
        }

        return $deSerializedResponse;
    }

    /**
     * Issue a GET request to the specified api method
     *
     * @param string $apiMethodRoute The route for the api method (e.g.
     *                               "repositories/$accountName/$repositoryName/deploy-keys")
     *
     * @return array The de-serialized response
     */
    public function delete($apiMethodRoute)
    {
        $headers = array('Accept' => 'application/json');
        $options = array('auth' => array($this->getUsername(), $this->getPassword()));
        $response = Requests::delete("https://bitbucket.org/api/1.0/$apiMethodRoute", $headers, $options);
        if (empty($response) || $response->success == false) {
            // the request was not successful
            return array();
        }

        $deSerializedResponse = json_decode($response->body, true);
        if (is_null($deSerializedResponse)) {
            // response could not be de-serialized
            return array();
        }

        return $deSerializedResponse;
    }

    /**
     * Get the current username
     *
     * @return string
     */
    private function getUsername()
    {
        return $this->username;
    }

    /**
     * Get the current password
     *
     * @return string
     */
    private function getPassword()
    {
        return $this->password;
    }
} 