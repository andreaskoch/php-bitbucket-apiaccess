<?php

namespace BitbucketApiAccess\Cmd\Actions;

use BitbucketApiAccess\Common;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Logout extends Command {
    const COMMAND_NAME = 'logout';

    /**
     * Configure the start command
     * @see Command
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('Logout')
            ->setHelp('The <info>' . self::COMMAND_NAME . '</info> log out of Bitbucket.');
    }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $bitbucketApi = Common\BitbucketApi::fromConfig();
        $bitbucketApi->clearCredentials();
    }
} 