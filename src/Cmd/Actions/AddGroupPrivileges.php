<?php

namespace BitbucketApiAccess\Cmd\Actions;

use BitbucketApiAccess\Common;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class AddGroupPrivileges extends Command
{
    const COMMAND_NAME = 'privileges:add';

    const ARGUMENT_NAME_ACCOUNT_NAME = "accountname";
    const ARGUMENT_NAME_GROUP_NAME = "groupname";
    const ARGUMENT_NAME_PRIVILEGE = "privilege";
    const ARGUMENT_NAME_REPOSITORIES = "repositories";

    /**
     * Configure the start command
     *
     * @see Command
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('List repositories')
            ->setHelp('The <info>' . self::COMMAND_NAME . '</info> list all repositories.')
            ->setDefinition(
                array(
                    new InputArgument(
                        self::ARGUMENT_NAME_ACCOUNT_NAME,
                        InputArgument::REQUIRED,
                        'The account name to which the group belongs to (e.g. "sample-org")'
                    ),
                    new InputArgument(
                        self::ARGUMENT_NAME_GROUP_NAME,
                        InputArgument::REQUIRED,
                        'The group name (e.g. "developers")'
                    ),
                    new InputArgument(
                        self::ARGUMENT_NAME_PRIVILEGE,
                        InputArgument::REQUIRED,
                        'The privilege (read, write, admin)'
                    ),
                    new InputArgument(
                        self::ARGUMENT_NAME_REPOSITORIES,
                        InputArgument::REQUIRED,
                        'The repositories to grant the group access to (e.g. \'[{"name":"<repo-name>","owner":"<owner-name>"}]\')'
                    )
                )
            );
    }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // account name
        $accountName = $input->getArgument(self::ARGUMENT_NAME_ACCOUNT_NAME);
        if (empty($accountName)) {
            $output->writeln("No account name supplied.");
            return;
        }

        // group name
        $groupName = $input->getArgument(self::ARGUMENT_NAME_GROUP_NAME);
        if (empty($groupName)) {
            $output->writeln("No group name supplied.");
            return;
        }

        // group
        $group = new Common\Group($accountName, $groupName);

        // privilege
        $privilege = $input->getArgument(self::ARGUMENT_NAME_PRIVILEGE);
        if (empty($groupName)) {
            $output->writeln("No privilege specified.");
            return;
        }

        // get input parameters
        $repositories = array();
        $repositoriesJson = $input->getArgument(self::ARGUMENT_NAME_REPOSITORIES);
        if (!empty($repositoriesJson)) {
            $repositories = json_decode($repositoriesJson, true);

            if (is_null($repositories)) {
                $output->writeln("Could not parse repositories json: $repositoriesJson");
                return;
            }
        }

        $api = Common\BitbucketApi::fromConfig();
        $groupPrivileges = new Common\GroupPrivileges($api);
        $response = $groupPrivileges->addGroupPrivilegesToRepositories($group, $repositories, $privilege);

        $output->writeln(json_encode($response, JSON_PRETTY_PRINT));
    }
} 