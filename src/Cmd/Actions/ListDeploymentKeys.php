<?php

namespace BitbucketApiAccess\Cmd\Actions;

use BitbucketApiAccess\Common;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class ListDeploymentKeys extends Command
{
    const COMMAND_NAME = 'keys:list';

    const ARGUMENT_NAME_REPOSITORIES = "repositories";

    /**
     * Configure the start command
     * @see Command
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('List deployment keys for the supplied repositories')
            ->setHelp('The <info>' . self::COMMAND_NAME . '</info> lists the deployment keys for the specified repositories.')
            ->setDefinition(
                array(
                    new InputArgument(
                        self::ARGUMENT_NAME_REPOSITORIES,
                        InputArgument::OPTIONAL,
                        'A json array of repositories to get the deployment keys for (e.g. \'[{"name":"<repo-name>","owner":"<owner-name>"}]\')'
                    )
                )
            );
    }

    /**
     * @see Command
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getArgument(self::ARGUMENT_NAME_REPOSITORIES)) {

            // read the the input from stdin
            $repositoryJson = "";
            while ($content = trim(fgets(STDIN))) {
                $repositoryJson .= $content;
            }
            $input->setArgument(self::ARGUMENT_NAME_REPOSITORIES, $repositoryJson);

        }
    }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // force interact for stdin handling even though the argument is optional
        $this->interact($input, $output);

        // get input parameters
        $repositories = array();
        $repositoriesJson = $input->getArgument(self::ARGUMENT_NAME_REPOSITORIES);
        if (!empty($repositoriesJson)) {
            $repositories = json_decode($repositoriesJson, true);

            if (is_null($repositories)) {
                $output->writeln("Could not parse repositories json: $repositoriesJson");
                return;
            }
        }

        if (empty($repositories)) {
            $output->writeln("No repositories specified");
            return;
        }

        // arrange
        $api = Common\BitbucketApi::fromConfig();
        $deploymentKeyAccess = new Common\DeploymentKeys($api);

        // act
        $keys = $deploymentKeyAccess->getAllDeploymentKeys($repositories);
        $output->writeln(json_encode($keys, JSON_PRETTY_PRINT));
    }
} 