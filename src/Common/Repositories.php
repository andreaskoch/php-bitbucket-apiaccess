<?php

namespace BitbucketApiAccess\Common;

class Repositories
{
    /** @var  BitbucketApi $bitbucketApi An instance of the bitbucket api */
    private $bitbucketApi;

    /** @var FilterInterface $repositoryFilter */
    private $repositoryFilter;

    /**
     * Creates a new instance of the Repositories class
     *
     * @param BitbucketApi $bitbucketApi An instance of the bitbucket api
     * @param FilterInterface $repositoryFilter A repository filter
     */
    public function __construct($bitbucketApi, $repositoryFilter)
    {
        if (is_null($bitbucketApi))
        {
            throw new \InvalidArgumentException("No bitbucket api supplied.");
        }

        if (is_null($repositoryFilter))
        {
            throw new \InvalidArgumentException("No repository filter supplied.");
        }

        $this->bitbucketApi = $bitbucketApi;
        $this->repositoryFilter = $repositoryFilter;
    }

    /**
     * Get all repositories (matching the supplied filter conditions)
     *
     * @param array $filterCondition A filter condition
     *
     * @throws \Exception In case no repositories were received
     *
     * @return array{Repository}
     */
    public function getAllRepositories($filterCondition = array())
    {
        $repositories = $this->bitbucketApi->get("user/repositories/");
        if (is_null($repositories))
        {
            // an error occurred
            throw new \Exception("No repositories received.");
        }

        $repositoryModels = array();
        foreach ($repositories as $repository)
        {
            $repositoryOwner = $repository["owner"];
            $repositoryName = $repository["slug"];

            /** @var  Repository $repository */
            $repositoryMatchesFilter = $this->repositoryFilter->isMatch($repository, $filterCondition);
            if (!$repositoryMatchesFilter) {
                continue;
            }

            $repo = new Repository($repositoryName, $repositoryOwner);
            $repositoryModels[] = $repo;
        }

        return $repositoryModels;
    }
} 