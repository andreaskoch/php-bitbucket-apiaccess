<?php

namespace BitbucketApiAccess\Cmd\Actions;

use BitbucketApiAccess\Common;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class ListRepositories extends Command {
    const COMMAND_NAME = 'repos:list';

    const ARGUMENT_NAME_FILTER = "filter";

    /**
     * Configure the start command
     * @see Command
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('List repositories')
            ->setHelp('The <info>' . self::COMMAND_NAME . '</info> list all repositories.')
            ->setDefinition(
                array(
                    new InputArgument(
                        self::ARGUMENT_NAME_FILTER,
                        InputArgument::OPTIONAL,
                        'A filter for the keys to return'
                    )
                )
            );
    }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // get input parameters
        $filterConditions = array();
        $filterText = $input->getArgument(self::ARGUMENT_NAME_FILTER);
        if (!empty($filterText))
        {
            $filterConditions = json_decode($filterText, true);

            if (is_null($filterConditions)) {
                $output->writeln("Could not parse filter expression: $filterText");
                return;
            }
        }

        // arrange
        $api = Common\BitbucketApi::fromConfig();
        $filter = new Common\PartialMatchRepositoryFilter();
        $repositoryAccess = new Common\Repositories($api, $filter);

        // act
        $keys = $repositoryAccess->getAllRepositories($filterConditions);
        $output->writeln(json_encode($keys, JSON_PRETTY_PRINT));
    }
} 