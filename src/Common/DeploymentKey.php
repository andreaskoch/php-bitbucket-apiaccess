<?php

namespace BitbucketApiAccess\Common;


class DeploymentKey
{
    /** @var string $repository */
    public $repository;

    /** @var string $id */
    public $id;

    /** @var string $label */
    public $label;

    /** @var string $key */
    public $key;

    /**
     * Creates a new instance of the DeploymentKey class.
     *
     * @param string $repository The repository name
     * @param string $id The key id
     * @param string $label The key label
     * @param string $key The public key
     *
     * @throws \InvalidArgumentException The $repository cannot be null or empty
     * @throws \InvalidArgumentException The $id cannot be null or empty
     * @throws \InvalidArgumentException The $label cannot be null or empty
     * @throws \InvalidArgumentException The $key cannot be null or empty
     */
    public function __construct($repository, $id, $label, $key)
    {
        if (empty($repository))
        {
            throw new \InvalidArgumentException("The repository cannot be null or empty");
        }

        if (empty($id))
        {
            throw new \InvalidArgumentException("The id cannot be null or empty");
        }

        if (empty($label))
        {
            throw new \InvalidArgumentException("The label cannot be null or empty");
        }

        if (empty($key))
        {
            throw new \InvalidArgumentException("The key cannot be null or empty");
        }

        $this->repository = $repository;
        $this->id = $id;
        $this->label = $label;
        $this->key = $key;
    }
} 