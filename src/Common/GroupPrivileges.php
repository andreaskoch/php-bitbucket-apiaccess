<?php

namespace BitbucketApiAccess\Common;

use SebastianBergmann\RecursionContext\InvalidArgumentException;

class GroupPrivileges
{
    /** @var BitbucketApi $bitbucketApi An instance of the bitbucket api */
    private $bitbucketApi;

    /**
     * Creates a new instance of the GroupPrivileges class
     *
     * @param BitbucketApi $bitbucketApi An instance of the bitbucket api
     */
    public function __construct($bitbucketApi)
    {
        if (is_null($bitbucketApi)) {
            throw new \InvalidArgumentException("No bitbucket api supplied.");
        }

        $this->bitbucketApi = $bitbucketApi;
    }

    /**
     * Get a list of the repositories on which a particular privilege group appears
     *
     * @see https://confluence.atlassian.com/display/BITBUCKET/group-privileges+Endpoint#group-privilegesEndpoint-GETalistofrepositorieswithaspecificprivilegegroup
     *
     * @param Group $group A user group
     *
     * @return array
     */
    public function getPrivilegesForGroup($group)
    {
        // https://bitbucket.org/api/1.0/group-privileges/{accountname}/{group_owner}/{group_slug}?filter=write
        $accountName = $group->getAccountName();
        $groupOwner = $group->getAccountName();
        $groupSlug = $group->getGroupName();
        $uri = "group-privileges/$accountName/$groupOwner/$groupSlug";

        $privileges = $this->bitbucketApi->get($uri);

        return $privileges;
    }

    /**
     * Grant group privileges on a repository
     *
     * @see https://confluence.atlassian.com/display/BITBUCKET/group-privileges+Endpoint#group-privilegesEndpoint-PUTgroupprivilegesonarepository
     *
     * @param Group      $group        A user group
     * @param Repository $repositories A list of repositories
     * @param string     $privilege    A privilege value (read, write, admin)
     *
     * @return array Responses
     * @throws InvalidArgumentException If no group was supplied.
     * @throws InvalidArgumentException If the supplied $privilege is null or empty.
     */
    public function addGroupPrivilegesToRepositories($group, $repositories, $privilege)
    {
        if (is_null($group)) {
            throw new \InvalidArgumentException("No group supplied");
        }

        if (empty($privilege)) {
            throw new \InvalidArgumentException("No privilege specified.");
        }

        $responses = array();

        foreach ($repositories as $repository) {
            $responses[] = $this->addGroupPrivilegesToRepository($group, $repository, $privilege);
        }

        return $responses;
    }

    /**
     * Grant group privileges on a repository
     *
     * @see https://confluence.atlassian.com/display/BITBUCKET/group-privileges+Endpoint#group-privilegesEndpoint-PUTgroupprivilegesonarepository
     *
     * @param Group      $group      A user group
     * @param Repository $repository A repository model
     * @param string     $privilege  A privilege value (read, write, admin)
     *
     * @return array Responses
     * @throws InvalidArgumentException If no group was supplied.
     * @throws InvalidArgumentException If the supplied $privilege is null or empty.
     */
    private function addGroupPrivilegesToRepository($group, $repository, $privilege)
    {
        if (is_null($group)) {
            throw new \InvalidArgumentException("No group supplied");
        }

        if (empty($privilege)) {
            throw new \InvalidArgumentException("No privilege specified.");
        }

        $accountName = $group->getAccountName();
        $repoSlug = $repository["name"];
        $groupOwner = $group->getAccountName();
        $groupSlug = $group->getGroupName();
        $uri = "group-privileges/$accountName/$repoSlug/$groupOwner/$groupSlug";

        return $this->bitbucketApi->put($uri, $privilege);
    }
}