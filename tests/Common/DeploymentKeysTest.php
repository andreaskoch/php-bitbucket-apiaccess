<?php

use BitbucketApiAccess\Common\DeploymentKeys;

class DeploymentKeysTest extends PHPUnit_Framework_TestCase
{
    /**
     * If no repositories are supplied; no keys are returned.
     *
     * @test
     * @return void
     */
    public function getAllDeploymentKeysNoRepositoriesNoKeys()
    {
        // arrange
        $bitbucketApi = $this->getMockBuilder('BitbucketApiAccess\Common\BitbucketApi')
            ->disableOriginalConstructor()
            ->getMock();

        $deploymentKeys = new DeploymentKeys($bitbucketApi);
        $repositories = array();

        // act
        $result = $deploymentKeys->getAllDeploymentKeys($repositories);

        // assert
        $this->assertEmpty($result, "The result should be empty");
    }

    /**
     * getAllDeploymentKeys returns a deployment key
     *
     * @test
     */
    public function getAllDeploymentKeysReturnsRepositoryKey()
    {
        // arrange
        $repository = new \BitbucketApiAccess\Common\Repository("temp-repo", "johndoe");
        $repositories = array($repository);

        $bitbucketApi = $this->getMockBuilder('BitbucketApiAccess\Common\BitbucketApi')
            ->disableOriginalConstructor()
            ->getMock();

        $bitbucketApi->expects($this->any())
            ->method('get')
            ->will($this->returnValue(
                array(
                    array("pk" => "a pk", "label" => "a label", "key" => "a key")
                )
            ));

        $deploymentKeys = new DeploymentKeys($bitbucketApi);

        // act
        $result = $deploymentKeys->getAllDeploymentKeys($repositories);

        // assert
        $this->assertNotEmpty($result, "The result should not be empty.");
    }

}
