<?php

namespace BitbucketApiAccess\Cmd\Actions;

use BitbucketApiAccess\Common;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class Login extends Command {
    const COMMAND_NAME = 'login';

    const ARGUMENT_NAME_USERNAME = "username";
    const ARGUMENT_NAME_PASSWORD = "password";

    /**
     * Configure the start command
     * @see Command
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('Login')
            ->setHelp('The <info>' . self::COMMAND_NAME . '</info> login to Bitbucket.')
            ->setDefinition(
                array(
                    new InputArgument(
                        self::ARGUMENT_NAME_USERNAME,
                        InputArgument::REQUIRED,
                        'The Bitbucket username'
                    ),
                    new InputArgument(
                        self::ARGUMENT_NAME_PASSWORD,
                        InputArgument::REQUIRED,
                        'The Bitbucket password'
                    )
                )
            );
    }

    /**
     * @see Command
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getArgument(self::ARGUMENT_NAME_USERNAME))
        {
            $dialog = $this->getHelperSet()->get('dialog');
            $username = $dialog->ask($output, sprintf("%s: ", self::ARGUMENT_NAME_USERNAME));
            $input->setArgument(self::ARGUMENT_NAME_USERNAME, $username);
        }

        if (!$input->getArgument(self::ARGUMENT_NAME_PASSWORD))
        {
            $dialog = $this->getHelperSet()->get('dialog');
            $password = $dialog->askHiddenResponse($output, sprintf("%s: ", self::ARGUMENT_NAME_PASSWORD));
            $input->setArgument(self::ARGUMENT_NAME_PASSWORD, $password);
        }
    }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument(self::ARGUMENT_NAME_USERNAME);
        $password = $input->getArgument(self::ARGUMENT_NAME_PASSWORD);

        $bitbucketApi = new Common\BitbucketApi($username, $password);
        if (!$bitbucketApi->canLogin()) {
            $output->writeln("Login failed.");
            return;
        }

        $output->writeln("Login succeeded.");
        $bitbucketApi->saveCredentials();
    }
} 