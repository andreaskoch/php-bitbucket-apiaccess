<?php

namespace BitbucketApiAccess\Common;

class Repository
{
    /** @var string $name The repository name */
    public $name;

    /** @var  string $owner The repository owner name */
    public $owner;

    /**
     * Creates a new instance of the Repository class.
     *
     * @param string $name  The repository name
     * @param string $owner The repository owner name
     *
     * @throws \InvalidArgumentException The $name cannot be null or empty
     * @throws \InvalidArgumentException The $owner cannot be null or empty
     */
    public function __construct($name, $owner)
    {
        if (empty($name)) {
            throw new \InvalidArgumentException("The name cannot be null or empty");
        }

        if (empty($owner)) {
            throw new \InvalidArgumentException("The owner cannot be null or empty");
        }

        $this->name = $name;
        $this->owner = $owner;
    }

    /**
     * Get the repository name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the repository owner name
     * @return string
     */
    public function getOwnerName()
    {
        return $this->owner;
    }
} 