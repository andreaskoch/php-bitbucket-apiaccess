<?php

namespace BitbucketApiAccess\Cmd\Actions;

use BitbucketApiAccess\Common;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Output\OutputInterface;

class AddDeploymentKey extends Command
{
    const COMMAND_NAME = 'keys:add';

    const ARGUMENT_NAME_LABEL = "label";
    const ARGUMENT_NAME_KEY = "key";
    const ARGUMENT_NAME_REPOSITORIES = "repositories";

    /**
     * Configure the start command
     * @see Command
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('Add a deployment key to the supplied repositories')
            ->setHelp('The <info>' . self::COMMAND_NAME . '</info> add the specified deployment key to all supplied repositories.')
            ->setDefinition(
                array(
                    new InputArgument(
                        self::ARGUMENT_NAME_LABEL,
                        InputArgument::REQUIRED,
                        'The label for the deployment key'
                    ),
                    new InputArgument(
                        self::ARGUMENT_NAME_KEY,
                        InputArgument::REQUIRED,
                        'The public part of your deployment key'
                    ),
                    new InputArgument(
                        self::ARGUMENT_NAME_REPOSITORIES,
                        InputArgument::OPTIONAL,
                        'A json array of repositories (e.g. \'[{"name":"<repo-name>","owner":"<owner-name>"}]\')'
                    )
                )
            );
    }

    /**
     * @see Command
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getArgument(self::ARGUMENT_NAME_REPOSITORIES)) {

            // read the the input from stdin
            $repositoryJson = "";
            while ($content = trim(fgets(STDIN))) {
                $repositoryJson .= $content;
            }
            $input->setArgument(self::ARGUMENT_NAME_REPOSITORIES, $repositoryJson);

        }
    }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // force interact for stdin handling even though the argument is optional
        $this->interact($input, $output);

        // get the label for the key
        $label = $input->getArgument(self::ARGUMENT_NAME_LABEL);
        if (empty($label)) {
            $output->writeln("<error>The label cannot be null or empty.</error>");
            return;
        }

        // get the deployment key
        $key = $input->getArgument(self::ARGUMENT_NAME_KEY);
        if (empty($key)) {
            $output->writeln("<error>The key cannot be null or empty.</error>");
            return;
        }

        // get the repositories to apply the key to
        $repositories = array();
        $repositoriesJson = $input->getArgument(self::ARGUMENT_NAME_REPOSITORIES);
        if (!empty($repositoriesJson)) {
            $repositories = json_decode($repositoriesJson, true);

            if (is_null($repositories)) {
                $output->writeln("Could not parse repositories json: $repositoriesJson");
                return;
            }
        }

        if (empty($repositories)) {
            $output->writeln("No repositories specified");
            return;
        }

        // arrange
        $api = Common\BitbucketApi::fromConfig();
        $deploymentKeyAccess = new Common\DeploymentKeys($api);

        // act
        $deploymentKeyAccess->addDeploymentKey($label, $key, $repositories);
    }
} 