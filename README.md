# Bitbucket API Access

A php-based library and command line tools for interacting with the Bitbucket API.

## Commands

- `repos:list`: List all repositories accessible by you
- `keys:list`: List all deployment keys for the repositories that are accessible by you

## Usage

Login:

```bash
app/console login
```

List all repositories that you have access to:

```bash
app/console repos:list
```

Logout:

```bash
app/console logout
```

List privileges for a group (`testgroup`) on the given account (`andreaskoch`):

```bash
privileges:bygroup "andreaskoch" "testgroup"
```

Add `read` (option are: read, write, admin) privileges for a group (`testgroup`) on a given list of repositories:

```bash
privileges:add "andreaskoch" "testgroup" "read" "[{\"name\": \"some-repo\", \"owner\": \"andreaskoch\"}]"
```