<?php

namespace BitbucketApiAccess\Cmd\Infrastructure;

use BitbucketApiAccess\Cmd\Actions;
use DI;
use Interop\Container\ContainerInterface;
use Symfony\Component\Console\Application;

class Container
{
    public static function initialize()
    {
        $builder = new DI\ContainerBuilder();

        // application
        $builder->addDefinitions([
            'application.name' => "Bitbucket ApiAccess Console",
            'application.version' => "0.1.0",
            Application::class => function (ContainerInterface $c) {
                $application = new Application($c->get("application.name"), $c->get("application.version"));
                $application->add($c->get(Actions\Login::class));
                $application->add($c->get(Actions\Logout::class));
                $application->add($c->get(Actions\ListDeploymentKeys::class));
                $application->add($c->get(Actions\AddDeploymentKey::class));
                $application->add($c->get(Actions\RemoveDeploymentKey::class));
                $application->add($c->get(Actions\ListRepositories::class));
                $application->add($c->get(Actions\ListGroupPrivileges::class));
                $application->add($c->get(Actions\AddGroupPrivileges::class));
                return $application;
            }
        ]);

        $container = $builder->build();

        return $container;
    }

}