<?php

namespace BitbucketApiAccess\Common;

class DeploymentKeys
{
    /** @var  BitbucketApi $bitbucketApi An instance of the bitbucket api */
    private $bitbucketApi;

    /**
     * Creates a new instance of the DeploymentKeys class
     *
     * @param BitbucketApi $bitbucketApi An instance of the bitbucket api
     *
     * @throw InvalidArgumentException If no $bitbucketApi is supplied.
     */
    public function __construct($bitbucketApi)
    {
        if (is_null($bitbucketApi)) {
            throw new \InvalidArgumentException("No bitbucket api supplied.");
        }

        $this->bitbucketApi = $bitbucketApi;
    }

    /**
     * Get all deployment keys for the given $repositories
     *
     * @param Repository[] $repositories An array of repositories
     *
     * @returns array{DeploymentKey} A list of deployment keys for the given $repositories
     */
    public function getAllDeploymentKeys($repositories = array())
    {
        $deploymentKeys = array();

        /** @var Repository $repository */
        foreach ($repositories as $repository) {

            $repositoryOwner = $repository->owner;
            $repositoryName = $repository->name;

            try {
                $keys = $this->getDeploymentKeys($repositoryOwner, $repositoryName);
            } catch (\Exception $deploymentKeyException) {
                error_log("{$deploymentKeyException->getMessage()}. Skipping repository.");
                continue;
            }

            foreach ($keys as $deploymentKey) {
                $deploymentKeys[] = $deploymentKey;
            }
        }

        return $deploymentKeys;
    }

    /**
     * Get all deployment keys for the specified repository
     *
     * @param string $accountName The repository account name
     * @param string $repositoryName The repository name
     *
     * @throws \InvalidArgumentException If the supplied $accountName is empty
     * @throws \InvalidArgumentException If the supplied $repositoryName is empty
     * @throws \Exception In case the api communication fails.
     *
     * @return array An array of deployment keys
     */
    public function getDeploymentKeys($accountName, $repositoryName)
    {
        if (empty($accountName)) {
            throw new \InvalidArgumentException("The account name cannot be null or empty");
        }

        if (empty($repositoryName)) {
            throw new \InvalidArgumentException("The repository name cannot be null or empty");
        }

        $keys = $this->bitbucketApi->get("repositories/$accountName/$repositoryName/deploy-keys/");
        if (is_null($keys)) {
            // an error occurred
            throw new \Exception("No deployment keys received for $accountName/$repositoryName");
        }

        // transform data structure
        $deploymentKeys = array();
        foreach ($keys as $key) {
            $repository = "$accountName/$repositoryName";
            $id = $key["pk"];
            $label = $key["label"];
            $key = $key["key"];

            $deploymentKeys[] = new DeploymentKey($repository, $id, $label, $key);
        }

        return $deploymentKeys;
    }

    /**
     * Add the supplied deployment keys to the specified repositories
     *
     * @param string $label The deployment key name
     * @param string $key The deployment key
     * @param array $repositories An array of repositories to apply the key to
     *
     * @throws \InvalidArgumentException If the supplied $label is empty
     * @throws \InvalidArgumentException If the supplied $key is empty
     */
    public function addDeploymentKey($label, $key, $repositories)
    {
        if (empty($label)) {
            throw new \InvalidArgumentException("The label cannot be null or empty");
        }

        if (empty($key)) {
            throw new \InvalidArgumentException("The key cannot be null or empty");
        }

        if (empty($repositories)) {
            return;
        }

        foreach ($repositories as $repository) {

            $repositoryOwner = $repository["owner"];
            $repositoryName = $repository["name"];

            $response = $this->bitbucketApi->post("repositories/$repositoryOwner/$repositoryName/deploy-keys/", array("label" => $label, "key" => $key));
            if (is_null($response)) {
                // an error occurred
                error_log("Could not add the deployment key \"$label\" to the repository \"$repositoryOwner/$repositoryName\"");
            }

        }
    }

    /**
     * Remove the specified deployment keys from the supplied repositories
     *
     * @param string $key The deployment key
     * @param array $repositories An array of repositories to apply the key to
     *
     * @throws \InvalidArgumentException If the supplied $key is empty
     */
    public function removeDeploymentKey($key, $repositories)
    {
        if (empty($key)) {
            throw new \InvalidArgumentException("The key cannot be null or empty");
        }

        if (empty($repositories)) {
            return;
        }

        foreach ($repositories as $repository) {
            $repositoryOwner = $repository["owner"];
            $repositoryName = $repository["name"];

            $deploymentKeys = $this->getDeploymentKeys($repositoryOwner, $repositoryName);
            foreach ($deploymentKeys as $deploymentKey) {
                /** @var DeploymentKey $deploymentKey */
                if ($deploymentKey->key !== $key) {
                    continue;
                }

                $response = $this->bitbucketApi->delete("repositories/$repositoryOwner/$repositoryName/deploy-keys/" . $deploymentKey->id);
                if (is_null($response)) {
                    // an error occurred
                    error_log("Could not delete the deployment key \"$key\" from the repository \"$repositoryOwner/$repositoryName\"");
                }

                echo "Removed deployment key \"{$deploymentKey->label} (Id: {$deploymentKey->id}\" from repository \"$repositoryOwner/$repositoryName\"\n";
            }
        }
    }
}