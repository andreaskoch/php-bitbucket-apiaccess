<?php

namespace BitbucketApiAccess\Common;

class UserPrivileges
{
    /** @var BitbucketApi $bitbucketApi An instance of the bitbucket api */
    private $bitbucketApi;

    /**
     * Creates a new instance of the UserPrivileges class
     *
     * @param BitbucketApi $bitbucketApi An instance of the bitbucket api
     */
    public function __construct($bitbucketApi)
    {
        if (is_null($bitbucketApi)) {
            throw new \InvalidArgumentException("No bitbucket api supplied.");
        }

        $this->bitbucketApi = $bitbucketApi;
    }

    /**
     * Get the privileges for the supplied $users and $rep
     *
     * @param User[] $users An list of users
     * @param Repository[] $repositories A list of repositories
     *
     * @return array
     */
    public function getPrivileges($users, $repositories)
    {
        $privileges = array();
        foreach($repositories as $repository)
        {
            foreach($users as $user)
            {
                $privileges[] = $this->getPrivilegesForRepository($user, $repository);
            }
        }

        return $privileges;
    }

    private function getPrivilegesForRepository($user, $repository)
    {

    }

}