<?php

namespace BitbucketApiAccess\Common;

/** Class Group represents a Bitbucket user group */
class Group
{
    /** @var string $groupName A group name (e.g. "developers") */
    private $groupName;

    /** @var string $accountName The group's account name (e.g. sample-org) */
    private $accountName;

    /**
     * Creates a new instance of the User class
     *
     * @param string $accountName The group's account name
     * @param string $groupName   A group name (e.g. "developers")
     */
    public function __construct($accountName, $groupName)
    {
        if (empty($accountName)) {
            throw new \InvalidArgumentException("The supplied account name cannot be null or empty");
        }

        if (empty($groupName)) {
            throw new \InvalidArgumentException("The supplied group name cannot be null or empty");
        }

        $this->accountName = $accountName;
        $this->groupName = $groupName;
    }

    /**
     * Get the account name of the group
     *
     * @return string
     */
    public function getAccountName()
    {
        return $this->accountName;
    }

    /**
     * Get the group name
     *
     * @return string
     */
    public function getGroupName()
    {
        return $this->groupName;
    }
}