<?php

namespace BitbucketApiAccess\Cmd;

use BitbucketApiAccess\Cmd\Actions;
use BitbucketApiAccess\Cmd\Infrastructure\Container;
use Symfony\Component\Console\Application;
use DI;

class Console {

    /**
     * Runs the console app
     */
    public static function run()
    {
        // initialize the di-container
        // auto-wire dependencies
        // $container = DI\ContainerBuilder::buildDevContainer();
        $container = Container::initialize();

        /** @var Application $application The symfony console application */
        $console = $container->get(Application::class);
        $console->run();
    }

} 