<?php

namespace BitbucketApiAccess\Common;

/** Class User represents a Bitbucket user */
class User
{
    /** @var string $username A username (e.g. "johndoe") */
    private $username;

    /**
     * Creates a new instance of the User class
     *
     * @param string $username A username (e.g. "johndoe")
     *
     * @throws \InvalidArgumentException If the supplied username is null or empty.
     */
    public function __construct($username)
    {
        if (empty($username))
        {
            throw new \InvalidArgumentException("The supplied username cannot be null or empty");
        }

        $this->username = $username;
    }

    /**
     * Get the username
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }
}