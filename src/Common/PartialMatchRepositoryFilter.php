<?php
/**
 * Created by PhpStorm.
 * User: vagrant
 * Date: 21.02.15
 * Time: 22:56
 */

namespace BitbucketApiAccess\Common;

class PartialMatchRepositoryFilter implements FilterInterface
{
    /**
     * Check whether the $repository matches the supplied $filter.
     *
     * @param Repository $repository A repository
     * @param array $condition An array of filter conditions
     *
     * @returns bool true if the $repository matches the supplied $filter; otherwise false
     */
    public function isMatch($repository, $condition)
    {
        if (is_null($repository)) {
            return false;
        }

        if (empty($condition)) {
            return true;
        }

        foreach ($condition as $name => $value) {

            switch (strtolower($name)) {

                case "name":
                    if (stripos($repository["name"], $value) === false)
                    {
                        return false;
                    }
                    break;

                case "owner":
                    if (stripos($repository["owner"], $value) === false)
                    {
                        return false;
                    }
                    break;
            }

        }

        return true;
    }
} 