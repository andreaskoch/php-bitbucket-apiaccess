<?php

namespace BitbucketApiAccess\Cmd\Actions;

use BitbucketApiAccess\Common;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ListGroupPrivileges extends Command
{
    const COMMAND_NAME = 'privileges:bygroup';

    const ARGUMENT_NAME_ACCOUNT_NAME = "accountname";
    const ARGUMENT_NAME_GROUP_NAME = "groupname";

    /**
     * Configure the start command
     *
     * @see Command
     */
    protected function configure()
    {
        $this->setName(self::COMMAND_NAME)
            ->setDescription('List repositories')
            ->setHelp('The <info>' . self::COMMAND_NAME . '</info> list all repositories.')
            ->setDefinition(
                array(
                    new InputArgument(
                        self::ARGUMENT_NAME_ACCOUNT_NAME,
                        InputArgument::REQUIRED,
                        'The account name to which the group belongs to (e.g. "sample-org")'
                    ),
                    new InputArgument(
                        self::ARGUMENT_NAME_GROUP_NAME,
                        InputArgument::REQUIRED,
                        'The group name (e.g. "developers")'
                    )
                )
            );
    }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // account name
        $accountName = $input->getArgument(self::ARGUMENT_NAME_ACCOUNT_NAME);
        if (empty($accountName)) {
            $output->writeln("No account name supplied.");
            return;
        }

        // group name
        $groupName = $input->getArgument(self::ARGUMENT_NAME_GROUP_NAME);
        if (empty($groupName)) {
            $output->writeln("No group name supplied.");
            return;
        }

        $group = new Common\Group($accountName, $groupName);
        $api = Common\BitbucketApi::fromConfig();
        $groupPrivileges = new Common\GroupPrivileges($api);
        $privileges = $groupPrivileges->getPrivilegesForGroup($group);

        $output->writeln(json_encode($privileges, JSON_PRETTY_PRINT));
    }
} 